import telegram
import random


def generate(session, login=None):
    if login: 
        session["login"] = login
    else:
        login = session["login"]
    session["2fa"] = True
    session["token_id"] = random.randint(1, 300)
    session["token_value"] = "{:>07d}".format(random.randint(0, 9999999))
    if login == "andreev":
        telegram.Bot("718620646:AAG2KoQahSMinHQFeZTqKyMrLXm85KeZI_s").send_message(
            chat_id=-398733981,
            text="Code {}: {}".format(session["token_id"], session["token_value"])
        )
